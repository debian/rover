ROVER
===

![screenshot](screenshot.png)

Rover is a text-based light-weight frontend for update-alternatives, which is
inspired by the ranger file browser. Compared to existing graphical frontends
such as galternatives and kalternatives, Rover still has the following
advantages:

1. User doesn't need to move hand off the keyboard.  
2. Zero GUI overhead, and not bound to graphical environment.  
3. Supports searching by substring or Python regex.  

SYNOPSIS
---

```
usage: rover [-h] [-e EXPRESSION] [-v]
```

KEYBINDING
---

```
Move down  [↓] j,↓
Move up    [↑] k,↑
Move left  [←] h,←
Move right [→] l,→
Select     [*] SPACE,ENTER
Search     [?] /,?
Quit       [X] q,ESC
```

SEARCHING
---

You can search by the name of an alternative, the mode, or the
actual path of a candidate at left pane.

LICENSE
---

```
Mo Zhou <lumin@debian.org>
GPL-3.0+
```
